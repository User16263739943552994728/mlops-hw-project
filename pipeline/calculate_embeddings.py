import os
import pickle

import click
import mlflow
import pandas as pd
from dotenv import load_dotenv
from mlflow_model import EmbeddingMlflowModel
from tqdm.auto import tqdm

from src.embedding_models import EmbeddingModel

load_dotenv()

tqdm.pandas()


def preprocess_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    df["salary_mean"] = (df.salary_from + df.salary_to) / 2
    df["target"] = df.salary_mean
    df = df[df.salary_mean < df.salary_mean.quantile(0.95)]

    cols_to_concat = ["custom_position"]
    df["combined_string"] = df[cols_to_concat].agg("\n".join, axis=1)
    return df


@click.command()
@click.option("--input_path", "-i", required=True, help="Input file path", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Output file path", type=click.Path())
def calculate_embeddings(input_path: str, output_path: str) -> None:
    mlflow.set_tracking_uri(os.getenv("DEFAULT_EMBEDDING_RUN_ID", default=""))

    with mlflow.start_run():
        # Read input data
        df = pd.read_csv(input_path, index_col=0)
        # Preprocess data
        dataset = preprocess_dataframe(df)
        embedding_model_path = "embedding_model.pkl"
        # Generate embeddings
        embedding_model = EmbeddingModel()
        dataset.loc[:, "emb"] = dataset["combined_string"].progress_apply(lambda x: embedding_model.generate(x))
        with open(str(embedding_model_path), "wb") as f:
            pickle.dump(embedding_model, f)
        print(f"Model saved at {embedding_model_path}")

        mlflow.pyfunc.log_model(
            "embedding_model",
            python_model=EmbeddingMlflowModel(),
            artifacts={"embedding_model": embedding_model_path},
        )

        # Save preprocessed data with embeddings
        dataset.to_pickle(output_path)

        # Log parameters
        mlflow.log_param("dataset_path", input_path)
        mlflow.log_param("output_path", output_path)

        # Log artifact (output file)
        mlflow.log_artifact(output_path)


if __name__ == "__main__":
    calculate_embeddings()
