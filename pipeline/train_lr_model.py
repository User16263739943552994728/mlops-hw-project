import os

import click
import mlflow
import pandas as pd
from dotenv import load_dotenv
from mlflow_model import LinearRegressionMlflowModel

from src.embedding_models import EmbeddingModel
from src.models import LinearRegressionModel

load_dotenv()


@click.command()
@click.option(
    "--dataset_path",
    "-i",
    required=True,
    help="Input dataset file path",
    type=click.Path(),
)
@click.option(
    "--output_path",
    "-o",
    required=True,
    help="Output file path",
    type=click.Path(),
)
def train_lr_model(dataset_path: str, output_path: str) -> None:
    mlflow.set_tracking_uri(os.getenv("DEFAULT_EMBEDDING_RUN_ID", default=""))

    with mlflow.start_run():
        # Read input dataset
        dataset = pd.read_pickle(dataset_path)

        # Initialize embedding model
        embedding_model = EmbeddingModel()

        # Train linear regression model
        lr_model = LinearRegressionModel(embedding_model=embedding_model)
        test_score = lr_model.train(dataset)
        lr_model.save_model(output_path)

        mlflow.pyfunc.log_model(
            "lr_model",
            python_model=LinearRegressionMlflowModel(),
            artifacts={"lr_model": output_path},
        )

        # Log parameters
        mlflow.log_param("dataset_path", dataset_path)
        mlflow.log_param("output_path", output_path)

        # Log metric
        mlflow.log_metric("lr_model_score", test_score)
        
        # Log artifact (model file)
        mlflow.log_artifact(output_path)


if __name__ == "__main__":
    train_lr_model()
