import os

import click
import mlflow
import pandas as pd
from dotenv import load_dotenv

from src.embedding_models import EmbeddingModel
from src.models import CatboostRegressionModel

load_dotenv()


@click.command()
@click.option(
    "--dataset_path",
    "-i",
    required=True,
    help="Input dataset file path",
    type=click.Path(),
)
@click.option(
    "--output_path",
    "-o",
    required=True,
    help="Output file path",
    type=click.Path(),
)
def train_catboost_model(dataset_path: str, output_path: str) -> None:
    mlflow.set_tracking_uri(os.getenv("DEFAULT_EMBEDDING_RUN_ID", default=""))

    with mlflow.start_run():
        # Read input dataset
        dataset = pd.read_pickle(dataset_path)

        # Initialize embedding model
        embedding_model = EmbeddingModel()

        # Train Catboost regression model
        catboost_model = CatboostRegressionModel(embedding_model=embedding_model)
        test_score = catboost_model.train(dataset)
        catboost_model.save_model(output_path)
        mlflow.catboost.log_model(catboost_model, "catboost_model")

        # Log parameters
        mlflow.log_param("dataset_path", dataset_path)
        mlflow.log_param("output_path", output_path)
        
        # Log metrics
        mlflow.log_metric("catboost_model_score", test_score)
        
        # Log artifact (model file)
        mlflow.log_artifact(output_path)


if __name__ == "__main__":
    train_catboost_model()
